"use strict";

function _templateObject() {
  var data = _taggedTemplateLiteral(["", ""]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

window.addEventListener("gamepadconnected", function (event) {
  console.log(event.gamepad);
  var gamepadEvent = event.gamepad;
  var gamepadObject = {
    gamepad: gamepadEvent.id,
    axes0: gamepadEvent.axes[0],
    axes1: gamepadEvent.axes[1],
    axes2: gamepadEvent.axes[2],
    axes3: gamepadEvent.axes[3],
    individualButtons: function individualButtons() {
      gamepadEvent.buttons.forEach(function (e) {
        return gamepadEvent.buttons(_templateObject(), e);
      });
    },
    // button0: gamepadEvent.buttons[0],
    // button1: gamepadEvent.buttons[1],
    // button2: gamepadEvent.buttons[2],
    // button3: gamepadEvent.buttons[3],
    // button4: gamepadEvent.buttons[4],
    // button5: gamepadEvent.buttons[5],
    // button6: gamepadEvent.buttons[6],
    // button7: gamepadEvent.buttons[7],
    // button8: gamepadEvent.buttons[8],
    // button9: gamepadEvent.buttons[9],
    // button10: gamepadEvent.buttons[10],
    // button11: gamepadEvent.buttons[11],
    // button12: gamepadEvent.buttons[12],
    // button13: gamepadEvent.buttons[13],
    // button14: gamepadEvent.buttons[14],
    // button15: gamepadEvent.buttons[15],
    // button16: gamepadEvent.buttons[16],
    vibrationMotor: gamepadEvent,
    vibrate: gamepadEvent.vibrationActuator
  };
  document.getElementById("gamepadNameHeader").innerHTML += gamepadEvent.id; // There are different vibration patterns

  navigator.getGamepads()[0].vibrationActuator.playEffect("dual-rumble", {
    startDelay: 0,
    duration: 150,
    weakMagnitude: 1.0,
    strongMagnitude: 1.0
  });
}); // ------ NOT WORKING - For detecting each button ------
// window.addEventListener("gamepadbuttondown", function (e) {
//   GamepadHapticActuator.buttonPressed(e);
//   let buttonPressed;
//   // Function for finding which button was pressed
//   if (gamepadEvent.buttons[0].pressed == true) {
//     console.log("A");
//   } else if (gamepadEvent.buttons[1].pressed == true) {
//     console.log("B");
//   } else if (gamepadEvent.buttons[2].pressed == true) {
//     console.log("X");
//   } else if (gamepadEvent.buttons[3].pressed == true) {
//     console.log("Y");
//   }
// });

updateGamepad = function (_updateGamepad) {
  function updateGamepad() {
    return _updateGamepad.apply(this, arguments);
  }

  updateGamepad.toString = function () {
    return _updateGamepad.toString();
  };

  return updateGamepad;
}(function () {
  requestAnimationFrame(updateGamepad);
  var gamepad = navigator.getGamepads()[0];
  if (!gamepad) return;
  console.log(gamepad); // for (let stats in gamepad) {
  //   if (gamepad) {
  //     console.log(gamepad[stats]);
  //   }
  // }
});

updateGamepad();